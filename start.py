#!/usr/bin/python
# Script with functions for starting servers
import subprocess as sub
import os

def machine_up(vm_ip):
  response = sub.call(["ping", "-c", "1", vm_ip], stdout=open(os.devnull, 'wb'))
  if response == 0:
    return True
  return False

def make_machine():
    #script to make a new VM
    return False

def deploy_container(minion, gamename, ip, port):
  # Make new game server via docker
  if gamename == "Warsow":
    #Ophalen, configureren en starten van Docker containers op minion, via saltstack
    salt = 'docker pull s7b4/warsow'
    sub.call(["salt", minion, "cmd.run", salt], stdout=open(os.devnull, 'wb'))
    salt = 'docker run --name '+str(port)+' -itd -p '+str(port)+':44400/udp \
       s7b4/warsow sv_hostname "Gamix Warsow server" \
      sv_maxclients "8" sv_skillevel "1" sv_iplimit "8" g_gametype "dm" g_teams_maxplayers "8"'
    sub.call(["salt", minion, "cmd.run", salt], stdout=open(os.devnull, 'wb'))
  elif gamename == "Teeworlds":
    salt = 'docker pull westtrade/teeworlds-server'
    sub.call(["salt", minion, "cmd.run", salt], stdout=open(os.devnull, 'wb'))
    salt = 'docker run --name '+str(port)+' -d -p 172.16.9.1:'+str(port)+':8303/udp westtrade/teeworlds-server'
    sub.call(["salt", minion, "cmd.run", salt], stdout=open(os.devnull, 'wb'))
  elif gamename == "OpenTTD":
    salt = 'docker pull redditopenttd/openttd'
    sub.call(["salt", minion, "cmd.run", salt], stdout=open(os.devnull, 'wb'))
    salt = 'docker run --name '+str(port)+' -d -p 172.16.9.1:'+str(port)+':3979/tcp \
      -p 172.16.9.1:'+str(port)+':3979/udp -e savegame=save1 -v /.openttd/save/:/config:rw  redditopenttd/openttd:latest'
    sub.call(["salt", minion, "cmd.run", salt], stdout=open(os.devnull, 'wb'))
  return True

def test_container():

  return True|False