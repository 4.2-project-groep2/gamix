#!/usr/bin/python
# Script for starting/stopping gameservers
# Run 'python gameserver.py -h' for options

import start as s
import gameserver_functions as f
import subprocess as sub
import os

args = f.arguments()

# Function to start game server
def startserver():
  f.start_output()
  # For now, hard-coded vm IP
  vm_ip="172.16.9.1"

  # Check if a vm exists or exit
  f.start_function("Checking if VM is up")
  machine_up = s.machine_up(vm_ip)
  f.run_function(machine_up, False)
  if not machine_up:
    #f.start_function("Making new VM")
    print("No connection to "+vm_ip)
    exit()

  #Insert into database and set status to registered
  port = f.return_port()
  f.start_function("Adding server to database")
  server_to_database = f.add_server_to_db(args.servername, args.game, 1, vm_ip, port, args.customerid)
  f.run_function(server_to_database, False)
  if not server_to_database:
    print("Failed to add server to database")
    exit()

  server_id = f.return_serverid(port)

  # Deploy the container
  game_name = f.return_game_name(args.game)
  f.start_function("Creating new container")
  container = s.deploy_container("game-vm", game_name, vm_ip, port)
  f.run_function(container, False)
  if not container:
    print("Failed to create container")
    f.start_function("Removing server from database")
    remove_server = f.remove_server_from_db(server_id)
    f.run_function(remove_server, False)
    exit()

 # Update the database status according to result
  f.start_function("Updating server status in database")
  db_status = f.update_status(2,server_id)
  f.run_function(db_status, False)

  return True    

def stopserver():
  f.start_output()

  minion = "game-vm"
  
  f.start_function("Stopping container")
  salt = 'docker container rm ' + args.port + ' -f'
  remove = sub.call(["salt", minion, "cmd.run", salt], stdout=open(os.devnull, 'wb'))
  f.run_function(remove, False)

  f.start_function("Update DB")
  remove_from_db = f.remove_server_from_db(args.serverid)
  f.run_function(remove_from_db, False)

  return True

if args.action == "start":
  f.start_function("Starting server")
  start_server = startserver()
  f.run_function(start_server, False)

elif args.action == "stop":
  f.start_function("Stopping server")
  stop_server = stopserver()
  f.run_function(stop_server, False)