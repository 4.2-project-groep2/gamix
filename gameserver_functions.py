#!/usr/bin/python
# Functions for the main script

import argparse
import sys
import mysql.connector
import ipaddress

# Connect to database
def connect_to_db():
  #UPDATE CREDENTIALS!!!
  cnx = mysql.connector.connect(host='localhost',database='gamix',user='gamix',password='lBwyzHtSJCUsB5P4')
  return cnx

# Close database connection
def close_db(cnx):
  cnx.close()

# Check if row exists in database
def check_row(id):
  cnx = connect_to_db()
  cursor = cnx.cursor(prepared=True)
  stmt = "SELECT Gameserver_ID FROM Gameserver WHERE Gameserver_ID = %s"
  cursor.execute(stmt, (id,))

  row_count = cursor.rowcount
  close_db(cnx)
  if row_count == 0:
    return False
  else:
    return True

def arguments():
  parser = argparse.ArgumentParser(description='Script to start or stop a game server. \
    To start a server, specify the server name, the game id and the customer id. \
    To stop a server, specify the server id and port number.')
  parser.add_argument("action", help="start/stop",type=start_stop)
  parser.add_argument("--servername", help="Servername")
  parser.add_argument("--game", help="Game number")
  parser.add_argument("--serverid", help="serverid") 
  parser.add_argument("--customerid", help="Customer ID")
  parser.add_argument("--ip", help="IP address")
  parser.add_argument("--port", help="Port number")
  args = parser.parse_args()

  if args.action == "start":
    if not args.servername or not args.game or not args.customerid:
      print("If you start a server, add a server name, game number and an ipadress")
      exit()
                        
  if args.action == "stop":
    if not args.serverid or not args.port:
      print("If you stop a server, add a serverid and a port number")
      exit()
        
  return args

def start_stop(value):
  if value not in ["stop","start"]:
    raise argparse.ArgumentTypeError("%s is not start or stop" % value)
  return value

# Return name of game
def return_game_name(game):
  try:
    cnx = connect_to_db()
    cursor = cnx.cursor(prepared=True)
    stmt = "SELECT Game_Name FROM Game WHERE Game_ID = %s"
    cursor.execute(stmt, (game,))
    name = cursor.fetchall()
    close_db(cnx)
    for array in name:
      for address in array:
        return address
  except:
    return False


# Return next IP address to use
def return_ip():
  try:
    cnx = connect_to_db()
    cursor = cnx.cursor()
    stmt = "SELECT Gameserver_IP FROM Gameserver ORDER BY Gameserver_IP DESC LIMIT 1"
    cursor.execute(stmt)
    for row in cursor.fetchall():
      for ip in row:
        new_ip = ipaddress.ip_address(ip) + 1
    close_db(cnx)
    return new_ip
  except:
    return False

def return_port():
  try:
    cnx = connect_to_db()
    cursor = cnx.cursor()
    stmt = "SELECT Gameserver_Port FROM Gameserver ORDER BY Gameserver_Port DESC LIMIT 1"
    cursor.execute(stmt)
    new_port = 0
    for row in cursor.fetchall():
      for port in row:
        new_port = port + 1
    close_db(cnx)
    if (new_port < 3899) or (new_port==0):
      new_port = 3899
    return new_port
  except:
    return False

def return_serverid(port):
  try:
    cnx = connect_to_db()
    cursor = cnx.cursor()
    stmt = "SELECT Gameserver_ID FROM Gameserver WHERE Game_Port = %s"
    cursor.execute(stmt, (port,))
    for row in cursor.fetchall():
      for id in row:
        return id
    close_db(cnx)
  except:
    return False

# Execute function to update status in database
def update_status(status, server_id):
  try:
    cnx = connect_to_db()
    cursor = cnx.cursor(prepared=True)
    stmt = "UPDATE Gameserver SET Gameserver_Status = %s WHERE Gameserver_ID = %s"
    cursor.execute(stmt, (status, server_id,))
    cnx.commit()
    close_db(cnx)
    return True
  except:
    return False

# Add new game server to database
def add_server_to_db(name, game, status, ip_address, port, customer_id):
  try:
    cnx = connect_to_db()
    cursor = cnx.cursor(prepared=True)
    stmt = "INSERT INTO Gameserver (Gameserver_Name, Gameserver_Game, Gameserver_Status, Gameserver_IP, Gameserver_Port, Gameserver_Customer) VALUES (%s, %s, %s, %s, %s, %s);"
    cursor.execute(stmt, (name, game, status, ip_address, port, customer_id))
    cnx.commit()
    close_db(cnx)
    return True
  except mysql.connector.Error as err:
    print("Something went wrong: {}".format(err))
    return False

# Remove server from database
def remove_server_from_db(server_id):
  try:
    cnx = connect_to_db()
    cursor = cnx.cursor(prepared=True)
    stmt = "DELETE FROM Gameserver WHERE Gameserver_ID = %s"
    cursor.execute(stmt, (server_id,))
    cnx.commit()
    close_db(cnx)
    return True
  except:
    return False

def start_output():
  sys.stdout.write( "|   [Status]    [Message]\n")

def start_function(message):
  sys.stdout.write( "|   [Running]   ")
  sys.stdout.write(message)
    
def run_function(status, exit):
  sys.stdout.flush()
  if status:
    sys.stdout.write("\r|   [Finished]\n")
  else:
    sys.stdout.write("\r|   [Failed...]\n")
    if exit:
      sys.stdout.write("The process is stopped")
      exit()