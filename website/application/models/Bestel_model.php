<?php
class bestel_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }



        public function nieuw()
        {
          $data = array(
                  'Gameserver_Name' => $this->input->post('naam'),
                  'Gameserver_Game' => $this->input->post('game'),
                  'Gameserver_Customer' => $_SESSION['user_id']
              );

              $this->db->insert('Gameserver', $data);
        }

        public function games()
        {
          $this->db->select('*');
          $this->db->from('Game');
          $query = $this->db->get();
          return $query->result_array();
        }


}
?>
