<?php
class dashboard_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }


        public function gameservers($id)
        {
          $this->db->select('*');
          $this->db->from('Gameserver');
          $this->db->join('Game', 'Gameserver.Gameserver_Game=Game_ID', 'left');
          $this->db->where('Gameserver_Customer', $id);
          $this->db->order_by('Gameserver_Game', 'DESC');
          $this->db->order_by('Gameserver_Created', 'ASC');
          $query = $this->db->get();
          return $query->result_array();
        }

        public function count($id)
        {
          $this->db->select('*');
          $this->db->from('Gameserver');
          $this->db->where('Gameserver_Customer', $id);
          $query = $this->db->get();
          return $query->num_rows();
        }



}
?>
