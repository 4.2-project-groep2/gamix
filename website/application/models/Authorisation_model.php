<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Authorisation_model extends CI_Model {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();
		$this->load->database();
		$this->load->helper('date');

	}

	/**
	 * create_user function.
	 *
	 * @access public
	 * @param mixed $username
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function create_user($username, $email, $password) {

		$data = array(
			'username'   => $username,
			'email'      => $email,
			'password'   => $this->hash_password($password),
			'created_at' => date('Y-m-j H:i:s'),
		);

		return $this->db->insert('users', $data);

	}

	/**
	 * resolve_user_login function.
	 *
	 * @access public
	 * @param mixed $username
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function resolve_user_login($username, $password) {

		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('username', $username);
		$hash = $this->db->get()->row('password');

		return $this->verify_password_hash($password, $hash);

	}

	/**
	 * get_user_id_from_username function.
	 *
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
	public function get_user_id_from_username($username) {

		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('username', $username);

		return $this->db->get()->row('id');

	}

	/**
	 * get_user function.
	 *
	 * @access public
	 * @param mixed $user_id
	 * @return object the user object
	 */
	public function get_user($user_id) {

		$this->db->from('users');
		$this->db->where('id', $user_id);
		return $this->db->get()->row();

	}


	public function set_login_details($user_id) {

		$data = array(
						'last_logged_in' => date('Y-m-d G:i:s'),
				);
		$this->db->where('id', $user_id);
		$this->db->update('users', $data);

	}

	/**
	 * hash_password function.
	 *
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password) {

		return password_hash($password, PASSWORD_BCRYPT);

	}

	/**
	 * verify_password_hash function.
	 *
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash) {

		return password_verify($password, $hash);

	}


        public function generateRandomString($length = 40) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return time().$randomString;
        }


        public function check_username($username) {
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('username', $username);

		return $this->db->get()->row('id');
	}

        public function register_password_reset($userid, $pwhash, $endtime) {

		$data = array(
                    'users_reset_pw_userid' => $userid,
                    'users_reset_pw_hash' => $pwhash,
                    'users_reset_pw_timelimit' => $endtime
		);

		return $this->db->insert('users_reset', $data);

	}

        public function generate_username($userid,$hash) {
		$this->db->select('*');
		$this->db->from('users_reset');
		$this->db->where('users_reset_pw_userid', $userid);
                $this->db->where('users_reset_pw_hash', $hash);
                $this->db->where('users_reset_pw_timelimit >', date('Y-m-d H:i:s'));
                $this->db->join('users', 'users.id = users_reset.users_reset_pw_userid', 'left');

		return $this->db->get()->row('username');
	}

        public function update_password($userid, $hash, $password) {
                $this->db->select('*');
		$this->db->from('users_reset');
		$this->db->where('users_reset_pw_userid', $userid);
                $this->db->where('users_reset_pw_hash', $hash);
                $this->db->where('users_reset_pw_timelimit >', date('Y-m-d H:i:s'));
                $this->db->join('users', 'users.id = users_reset.users_reset_pw_userid', 'left');

		if(!empty($this->db->get()->row('email'))){
                    $data = array(
                        'password' => $this->hash_password($password)
                    );

                    $this->db->where('id', $userid);
                    $this->db->update('users', $data);



                    $this->db->where('users_reset_pw_hash', $hash);
                    return $this->db->delete('users_reset');

                }else{
                    $this->session->set_flashdata('codeunvalid', 'notok');
                    redirect(base_url('authorisatie/inloggen'));
                }

	}

		public function register() {

			$data = array(
                  'username' => $this->input->post('username'),
                  'firstname' => $this->input->post('firstname'),
                  'surname' => $this->input->post('lastname'),
                  'email' => $this->input->post('email'),
                  'password' => $this->hash_password($this->input->post('password')),
                  'created_at' => date('Y-m-j H:i:s')
              );

              $this->db->insert('users', $data);

		}

}
