<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="signin-wrapper">

  <div class="signin-box mg-b-50">
    <h2 class="slim-logo">Gamix</h2>
    <h2 class="signin-title-primary">Registreren</h2>
    <p class="mg-b-20">Vul de gegevens in om te registreren bij Gamix.</p>
    <?php if (validation_errors()) : ?>
      <div class="alert alert-outline alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <?= validation_errors() ?>
      </div><!-- alert -->
        <?php endif; ?>
        <?php if (isset($error)) : ?>
          <div class="alert alert-outline alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <?= $error ?>
          </div><!-- alert -->
        <?php endif; ?>

    <?php echo form_open('', array('class' => 'm-t-20')); ?>
    <div class="form-group">
      <input type="text" id="username" name="username" class="form-control" placeholder="Gebruikersnaam" required>
    </div><!-- form-group -->
    <div class="form-group ">
      <input type="password" id="password" name="password" class="form-control" placeholder="Wachtwoord" required>
    </div><!-- form-group -->
    <div class="form-group">
      <input type="text" id="firstname" name="firstname" class="form-control" placeholder="Voornaam" required>
    </div><!-- form-group -->
    <div class="form-group">
      <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Achternaam" required>
    </div><!-- form-group -->
    <div class="form-group mg-b-30">
      <input type="text" id="email" name="email" class="form-control" placeholder="E-mail adres" required>
    </div><!-- form-group -->


    <button class="btn btn-primary btn-block btn-signin mg-b-30" type="submit">Registreren</button>
    </form>
  </div><!-- signin-box -->

</div><!-- signin-wrapper -->
