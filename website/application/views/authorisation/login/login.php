<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="signin-wrapper">

  <div class="signin-box mg-b-50">
    <h2 class="slim-logo">Gamix</h2>
    <h2 class="signin-title-primary">Klantenportal</h2>
    <p class="mg-b-20">Log in om toegang te krijgen.</p>
    <?php if (validation_errors()) : ?>
      <div class="alert alert-outline alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <?= validation_errors() ?>
      </div><!-- alert -->
        <?php endif; ?>
        <?php if (isset($error)) : ?>
          <div class="alert alert-outline alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <?= $error ?>
          </div><!-- alert -->
        <?php endif; ?>

        <?php
        if(isset($_SESSION['signoutmessage'])):
            if($_SESSION['signoutmessage'] == 'signedout'): ?>
            <div class="alert alert-outline alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              U bent succesvol uitgelogd. Tot de volgende keer!
            </div><!-- alert -->
            <?php endif;
			if($_SESSION['signoutmessage'] == 'registered'): ?>
            <div class="alert alert-outline alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              U bent succesvol geregistreerd! Log in om door te gaan.
            </div><!-- alert -->
            <?php endif;
        endif; ?>

    <?php echo form_open('', array('class' => 'm-t-20')); ?>
    <div class="form-group">
      <input type="text" id="username" name="username" class="form-control" placeholder="Gebruikersnaam" >
    </div><!-- form-group -->
    <div class="form-group mg-b-30">
      <input type="password" id="password" name="password" class="form-control" placeholder="Wachtwoord">
    </div><!-- form-group -->
    <button class="btn btn-primary btn-block btn-signin mg-b-30" type="submit">Inloggen</button>
    </form>
    <p class="mg-b-0">Nog geen account? <a href="<?php echo base_url(); ?>authorisatie/registreren">Registreren</a></p>
  </div><!-- signin-box -->

</div><!-- signin-wrapper -->
