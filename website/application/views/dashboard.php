<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


  <div class="row">
            <div class="col-lg-6">
              <p>Het opzetten van een gameserver was nog nooit zo makkelijk. Met één klik op de knop bestel je bij ons gemakkelijk een gameserver. Wij doen alles. Het enige wat jezelf moet doen is spelen ;-)</p>
              <div class="row row-sm">
                <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                  <a href="<?php echo base_url(); ?>bestel" class="btn btn-success btn-block">Bestel een gameserver!</a>
                </div><!-- col-6 -->
              </div>


            </div><!-- col-6 -->
            <div class="col-lg-6 mg-t-20 mg-sm-t-30 mg-lg-t-0">
              <div class="row">
                <div class="col-sm-6 mg-l-auto">
                  <div class="card card-earning-summary tx-right">
                    <h6>Aantal gameservers</h6>
                    <h1><?php echo $count; ?>x</h1>
                  </div><!-- card -->
                </div><!-- col-6 -->

              </div><!-- row -->

            </div><!-- col-6 -->
          </div>


          <div class="card card-table mg-t-20 mg-sm-t-30">
                  <div class="card-header">
                    <h6 class="slim-card-title">Mijn gameservers</h6>
                  </div><!-- card-header -->
                  <div class="table-responsive">
                    <table class="table mg-b-0 tx-13">
                      <thead>
                        <tr class="tx-10">
                          <th class="wd-10p pd-y-5 tx-center"></th>
                          <th class="pd-y-5">Servernaam</th>
                          <th class="pd-y-5">Game</th>
                          <th class="pd-y-5">IP-adres</th>
                          <th class="pd-y-5">Poort</th>
                          <th class="pd-y-5">Aangemaakt op</th>
                          <th class="pd-y-5 tx-center">Acties</th>
                        </tr>
                      </thead>
                      <tbody>

                          <?php foreach ($gameservers as $gameserver): ?>
                            <tr>
                                <td class="tx-center"><img src="<?php echo base_url(); ?>assets/images/games/<?php echo $gameserver['Game_Image'] ?>" class="wd-55" alt="Image"></td>
                                <td class="valign-middle"><?php echo $gameserver['Gameserver_Name'] ?></td>
                                <td class="valign-middle"><?php echo $gameserver['Game_Name'] ?></td>
                                <td class="valign-middle"><?php echo $gameserver['Gameserver_IP'] ?></td>
                                <td class="valign-middle"><?php echo $gameserver['Gameserver_Port'] ?></td>
                                <td class="valign-middle"><?php echo edit_datetime(date("d F Y \- H:i:s",strtotime($gameserver['Gameserver_Created']))); ?></td>
                                <td class="valign-middle tx-center">
                                  <a href="" class="tx-gray-600 tx-24" data-toggle="modal" data-target="#verwijder<?php echo $gameserver['Gameserver_ID']; ?>"><i class="icon ion-trash-a"></i></a>
                                </td>
                            </tr>
                          <?php endforeach; ?>
                      </tbody>
                    </table>
                    <?php if(!$gameservers){ ?>
                      <div class="tx-center mg-t-50 mg-b-50">Nog geen gameserver aangemaakt.</div>
                    <?php }?>
                  </div><!-- table-responsive -->
                </div>
				<br>
				<br>
				<br>
  </div> <!-- container -->

  <!-- Deactiveer gebruiker -->
  <?php foreach ($gameservers as $gameserver): ?>
  <div class="modal fade" id="verwijder<?php echo $gameserver['Gameserver_ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">

              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Verwijder gameserver</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <?php echo form_open_multipart(); ?>

                        <div class="row">
                           <div class="col-12">
                             <p align="center"><i class="ion-alert" style="font-size: 150px; color:#ff5d48;"></i></p>
                               <div class="alert alert-danger" role="alert">
                                   Let op! Hiermee verwijdert u deze gameserver. Het is niet mogelijk om deze actie te herstellen.
                               </div>
                           </div>
                        </div>
                      </div>



            </div></div>
              <div class="modal-footer">
                  <input type="hidden" required parsley-type="name" class="form-control" id="type_actie" name="type_actie" value="verwijder">
                  <input type="hidden" required parsley-type="name" class="form-control" id="verwijder_id" name="verwijder_id" value="<?php echo $gameserver['Gameserver_ID'] ?>">
                  <input type="hidden" required parsley-type="name" class="form-control" id="port" name="port" value="<?php echo $gameserver['Gameserver_Port'] ?>">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuleren</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-trash"></i></span>
                       Verwijderen
                    </button>
              </div>
            </form>
          </div>
      </div>
  </div></div>
  <?php endforeach; ?>
