<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="section-wrapper" >

<!-- Row start -->
<div class="row">
    <div class="col-md-12">
        <div class="card-box">

            <div class="row">

                <div class="col-md-6">
                  <h4 class="section-title mg-t-0"><i class="icon ion-person"></i> Gameserver</h4>
                    <?php echo form_open_multipart(); ?>
                    <div class="form-group row">
                      <label for="example-text-input" class="col-3 col-form-label">Game: </label>
                      <div class="col-9">
                        <select class="form-control" name="game">
                          <?php foreach ($games as $game): ?>
                            <option value="<?php echo $game['Game_ID'] ?>"><?php echo $game['Game_Name'] ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-text-input" class="col-3 col-form-label">Naam gameserver: </label>
                      <div class="col-9">
                          <input class="form-control" type="text" name="naam">
                      </div>
                    </div>
                    <br>
                </div> <!-- col-md-6 -->



                <div class="col-md-6">
                  <h4 class="section-title mg-t-0"><i class="ion-android-home"></i> Hoe werkt het?</h4>
                          Het aanmaken van een gameserver is heel gemakkelijk! Kies aan de linkerkant welke game je wilt spelen, geef een leuke naam en druk op bestellen.<br><br>
                          Wij zorgen dat alles draait zodat jij zonder zorgen kan gamen.
                    </div>

                </div> <!-- col-md-6 -->
            </div> <!-- row -->

            <div class="row">
              <div class="col-md-12" align="right">
                <input type="hidden" required parsley-type="name" class="form-control" id="type_actie" name="type_actie" value="nieuw">
                <button type="submit" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#loading">
                   Bestellen
                </button>

              </div>
            </div> <!-- row -->



        </div> <!--card-box -->
    </div> <!-- col-md-12 -->

</div> <!-- row -->



</div> <!-- container -->


<!-- Modal -->
<div id="loading" class="modal fade" role="dialog">
  <div class="modal-dialog tx-white tx-center">
          <div class="tx-center">
                <i class="ion-clock tx-120 tx-white"></i> <br>
                De gameserver wordt aangemaakt. <br>
                Een moment geduld alstublieft...
          </div>
  </div>
</div>
