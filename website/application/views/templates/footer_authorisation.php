<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

        <script src="<?php echo base_url(); ?>template/app/lib/jquery/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/popper.js/js/popper.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/bootstrap/js/bootstrap.js"></script>

        <script src="<?php echo base_url(); ?>template/app/js/slim.js"></script>
    </body>
</html>
