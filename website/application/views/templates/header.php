<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if(!isset($menuid)):
    $menuid = 99999;
endif;?>
<?php if(!isset($title)):
    $title = "Welkom";
endif;?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.9, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


        <!-- App title -->
        <title><?php echo $title; ?> - Gamix</title>

        <!-- vendor css -->
        <link href="<?php echo base_url(); ?>template/app/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>template/app/lib/Ionicons/css/ionicons.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>template/app/lib/jquery-toggles/css/toggles-full.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>template/app/lib/jt.timepicker/css/jquery.timepicker.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>template/app/lib/spectrum/css/spectrum.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>template/app/lib/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>template/app/lib/datatables/css/jquery.dataTables.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>template/app/lib/select2/css/select2.min.css" rel="stylesheet">

        <!-- Switchery css -->
        <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>template/app/lib/SpinKit/css/spinkit.css" rel="stylesheet">


        <!-- Slim CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/css/slim.css">

        <!-- CK Editor -->
        <script src="//cdn.ckeditor.com/4.9.1/basic/ckeditor.js"></script>
        <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
	
	
    </head>


    <body>

        <!-- Navigation Bar-->
        <div class="slim-header">
          <div class="container">
            <div class="slim-header-left">
              <a href="<?php echo base_url(); ?>index.php" class="logo">
                  <h2 class="slim-logo">Gamix</h2>
                  <span></span>
              </a>

            </div><!-- slim-header-left -->
            <div class="slim-header-right">

              <div class="">
                <a href="" class="logged-user">
                  <img src="<?php echo base_url(); ?>assets/images/users/default.jpg" alt="">
                  <span>@<?php echo $_SESSION['username']; ?></span>
                </a>

              </div><!-- dropdown -->
            </div><!-- header-right -->
          </div><!-- container -->
        </div><!-- slim-header -->


        <div class="slim-navbar">
          <div class="container">
            <ul class="nav">
              <li class="nav-item <?php if($menuid == 99999): echo 'active';endif;?>">
                <a class="nav-link" href="<?php echo base_url(); ?>">
                  <i class="icon ion-ios-home"></i>
                  <span>Dashboard</span>
                </a>

              </li>
              <li class="nav-item <?php if($menuid == 1): echo 'active';endif;?>">
                <a class="nav-link" href="<?php echo base_url(); ?>bestel">
                  <i class="icon fa fa-shopping-cart"></i>
                  <span> Bestel gameserver</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>authorisatie/uitloggen">
                  <i class="icon fa fa-sign-out"></i>
                  <span>Uitloggen</span>
                </a>
              </li>
            </ul>
          </div><!-- container -->
        </div><!-- slim-navbar -->

        <div class="slim-mainpanel">
          <div class="container">
            <div class="slim-pageheader ">
              <table class="wd-100p">
                <tr>
              <td class="">
              <h6 class="slim-pagetitle"><?php echo $title; ?></h6>

              <ol class="breadcrumb slim-breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home </a></li>
                <?php if(!empty($broodkruimel)){ echo $broodkruimel; } ?>
                <li class="breadcrumb-item active" aria-current="page"><?php if(!isset($breadcrumb_title)){echo $title;}else{echo $breadcrumb_title;} ?></li>
              </ol>
            </td>
            <?php if(!empty($menu)): ?>
              <td class="wd-20p mobile" align="right">
                <button class="btn btn-primary btn-block mg-b-10 wd-100 mg-t-10 mobile" type="button" id="acties" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Acties <i class="ion-chevron-down mg-l-5"></i></button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acties">
                  <?php echo $menu; ?>
                </div><!-- dropdown-menu -->
              </td>
          <?php endif; ?>
        </tr>
            </table>
            </div><!-- slim-pageheader -->

            <?php if(isset($_SESSION['succesupdate'])): ?>
             <div class="row">
                <div class="col-12">
                    <div class="alert alert-success" role="succes">
                        <?php echo $_SESSION['succesupdate']; ?>
                    </div>
                </div>
             </div>
            <?php endif; ?>

            <?php if(isset($_SESSION['errorupdate'])): ?>
             <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?php echo $_SESSION['errorupdate']; ?>
                    </div>
                </div>
             </div>
            <?php endif; ?>
