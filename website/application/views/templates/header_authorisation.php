<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="">
        <title>Gamix - Login</title>

        <!-- Slim CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/app/css/slim.css">
      </head>

      <body class="login-hmc-bg" style="background-image: url('<?php echo base_url(); ?>assets/images/bg.jpg');">
