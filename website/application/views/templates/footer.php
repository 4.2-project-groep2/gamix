<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

</div><!-- container -->
</div><!-- slim-mainpanel -->



            <!-- End Footer -->
        <script>
            var resizefunc = [];
        </script>

        <script src="<?php echo base_url(); ?>template/app/lib/jquery/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/popper.js/js/popper.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/bootstrap/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/jquery-toggles/js/toggles.min.js"></script>
        <script src="<?php echo base_url(); ?>template/app/js/slim.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/jquery.cookie/js/jquery.cookie.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/jt.timepicker/js/jquery.timepicker.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/spectrum/js/spectrum.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/jquery.maskedinput/js/jquery.maskedinput.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/datatables/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/datatables-responsive/js/dataTables.responsive.js"></script>
        <script src="<?php echo base_url(); ?>template/app/lib/select2/js/select2.min.js"></script>


        <!-- Switchery -->
        <script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>

        <!-- Dropify -->
        <script src="<?php echo base_url(); ?>assets/plugins/fileuploads/js/dropify.min.js"></script>



        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>

        <?php if(isset($footerscript)):
            echo $footerscript;
        endif; ?>

    </body>
</html>
