<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper('url_helper');
                $this->load->model('bestel_model');
                $this->load->model('dashboard_model');
                $this->load->library('form_validation');
        }


	public function index()
	{
                check_login();

                $this->form_validation->set_rules('type_actie', 'type_actie', 'required');

                if ($this->form_validation->run() === FALSE)
                {
                    $data['title'] = "Welkom, ".$_SESSION['firstname']." ".$_SESSION['surname'];
                    $data['breadcrumb_title'] = "Dashboard";

                    $data['gameservers'] = $this->dashboard_model->gameservers($_SESSION['user_id']);
                    $data['count'] = $this->dashboard_model->count($_SESSION['user_id']);

                    $this->load->view('templates/header', $data);
    		            $this->load->view('dashboard', $data);
    				        $this->load->view('templates/footer', $data);

                } else {

                  if($this->input->post('type_actie') == 'verwijder'){
                    $id = $this->input->post('verwijder_id');
                    $port = $this->input->post('port');
                    $check = exec('python /scripts/gamix/gameserver.py stop --serverid '.$id.' --port '.$port);
            				$this->session->set_flashdata('succesupdate', 'De gameserver verwijderd!');
                    redirect('dashboard');

                  }
                }
	}

	public function bestel()
	{

    $this->form_validation->set_rules('type_actie', 'type_actie', 'required');

    if ($this->form_validation->run() === FALSE)
    {

                check_login();

                $data['title'] = "Bestel een gameserver";
                $data['breadcrumb_title'] = "Bestel";
                $data['menuid'] = 1;

                $data['games'] = $this->bestel_model->games();

                $this->load->view('templates/header', $data);
		            $this->load->view('bestel', $data);
				        $this->load->view('templates/footer', $data);


    } else {

      if($this->input->post('type_actie') == 'nieuw'){



        $gs_name = $this->input->post('naam');

        if(empty($gs_name)){
          $gs_name = 'Mijn gameserver';
        }

				$gs_game = $this->input->post('game');
				$gs_customer = $_SESSION['user_id'];
				$check = exec('python /scripts/gamix/gameserver.py start --servername "'.$gs_name.'" --game '.$gs_game.' --customerid '.$gs_customer);

        if($check == True){
					$this->session->set_flashdata('succesupdate', 'De server is succesvol aangemaakt!');
          redirect('dashboard');
				} else {
					$this->session->set_flashdata('errorupdate', 'Oeps, er is iets fout gegaan...'.$check);
          redirect('bestel');
				}
      }

    } //elseform

	}






}
