<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 *
 * @extends CI_Controller
 */
class Authorisation extends CI_Controller {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->helper('date');
		$this->load->model('authorisation_model');

	}


	public function index() {



	}


	/**
	 * login function.
	 *
	 * @access public
	 * @return void
	 */
	public function login() {

                if (isset($_SESSION['in_logged']) && $_SESSION['in_logged'] === true)
                {
                    redirect('');
                }

		$data = new stdClass();

		$this->load->helper('form');
		$this->load->library('form_validation');


		$this->form_validation->set_rules('username', 'gebruikersnaam', 'trim|required');
		$this->form_validation->set_rules('password', 'wachtwoord', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header_authorisation');
			$this->load->view('authorisation/login/login');
			$this->load->view('templates/footer_authorisation');
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if ($this->authorisation_model->resolve_user_login($username, $password)) {

				$user_id = $this->authorisation_model->get_user_id_from_username($username);
				$user    = $this->authorisation_model->get_user($user_id);

				// set session user datas
				$_SESSION['user_id']      = (int)$user_id;
				$_SESSION['username']     = (string)$user->username;
				$_SESSION['firstname']     = (string)$user->firstname;
				$_SESSION['surname']     = (string)$user->surname;
				$_SESSION['in_logged']    = (bool)true;

				//Schrijf IP en Datum
				$this->authorisation_model->set_login_details($user_id);

				if (isset($_SESSION['page']))
				{
					redirect($_SESSION['page']);
				} else {
					redirect(base_url());
				}


			} else {
				$data->error = 'Gebruikersnaam of wachtwoord mogelijk onjuist.';

				$this->load->view('templates/header_authorisation');
				$this->load->view('authorisation/login/login', $data);
				$this->load->view('templates/footer_authorisation');

			}

		}

	}

	/**
	 * logout function.
	 *
	 * @access public
	 * @return void
	 */
	public function logout() {

		// create the data object
		$data = new stdClass();

		if (isset($_SESSION['in_logged']) && $_SESSION['in_logged'] === true) {

			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}

			$this->session->set_flashdata('signoutmessage', 'signedout');

			redirect(base_url('authorisatie/inloggen'));

		} else {

			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			redirect('/');

		}

	}

	public function register() {

								if (isset($_SESSION['in_logged']) && $_SESSION['in_logged'] === true)
								{
										redirect('');
								}

		$data = new stdClass();

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'gebruikersnaam', 'trim|required');
		$this->form_validation->set_rules('password', 'wachtwoord', 'required');
		$this->form_validation->set_rules('firstname', 'Voornaam', 'required');
		$this->form_validation->set_rules('lastname', 'Achternaam', 'required');
		$this->form_validation->set_rules('email', 'E-mail', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header_authorisation');
			$this->load->view('authorisation/register/register');
			$this->load->view('templates/footer_authorisation');
		} else {

			if(!$this->authorisation_model->check_username($this->input->post('username'))){

				$this->authorisation_model->register();
				$this->session->set_flashdata('signoutmessage', 'registered');
				redirect("authorisatie/inloggen");

			} else {
				$data->error = 'Dit gebruikersnaam bestaat al!';

				$this->load->view('templates/header_authorisation');
				$this->load->view('authorisation/register/register', $data);
				$this->load->view('templates/footer_authorisation');

			 }

		}

	}

}
