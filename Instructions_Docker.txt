Game dockers:
	https://hub.docker.com/r/s7b4/warsow/
	https://hub.docker.com/r/virtualroot/docker-teeworlds/
	https://hub.docker.com/r/bateau/openttd/


docker pull s7b4/warsow:latest && 
	docker pull virtualroot/docker-teeworlds:latest && 
	docker pull bateau/openttd:latest


## Warsow
# For custom config:
# Create a custom config my-config.cfg
# Add -v /path/to/my-config.cfg:/etc/warsow/default.cfg

# Replace -p 44400:44400/udp -p 44444:44444 --> -P for random ports
docker tun -d -p 44400:44400/udp -p 44444:44444 s7b4/warsow:latest


## Teeworlds
# Just start with chosen Interface/IP
docker run -d virtualroo/docker-teeworlds:latest


## OpenTTD
# create a .openttd-folder for persistent data
# create the openttd.cfg inside this folder

# Replace -p 3979:3979/tcp -p 3979:3979/udp --> -P for random ports
# In openttd.cfg set autosave_on_exit=true
# Add -e loadgame=exit to auto load save
docker run -d -p 3979:3979/tcp -p 3979:3979/udp -v /your/path/to/.openttd bateau/openttd:latest
